<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'pricaalmeida' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'root' );

/** Nome do host do MySQL */
define( 'DB_HOST', '192.168.0.39' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3!UV,t5wDX!7Txrjg7^)!dnhq>mF)?l[9Ah5j$$*dkydKUDFkT}=k3v%C^o$b|)Q' );
define( 'SECURE_AUTH_KEY',  'A{IPs$Q{*sGdy@wnhtwhy<ly)ja$gZt6NTOlrv&s[|O8 :)I:>uv$8trtqe&o$7d' );
define( 'LOGGED_IN_KEY',    '0^ z6-UdGtp]bPjxGf%Qx}.}A*%.&kc~|#o*stz=dFh{{1lEa4Amp9:sBB;GKYCo' );
define( 'NONCE_KEY',        'G+@=1H@[rD6wFM@8|[RD8/lo<sQKSCuOZR_D/!vNUuDNVuf*?Awrx}[JR8C/k!bB' );
define( 'AUTH_SALT',        '-4$~6RV-<VG8|JkTH!idNkiJ0@yug#>|NBKsW-Ej`x tKf}154Q/sfuHf+)/Myde' );
define( 'SECURE_AUTH_SALT', '6rprF@=D>jFt*+arK>V+_;c~bg}6Uu7OCo__PYB$<,/mxRkNXiB~.(.oZ~ST~6{Q' );
define( 'LOGGED_IN_SALT',   'MgQWxSetNXy|8.9d&{3+j$F88eSi9(y^!0dV=|,[jY#se}TZZnI?ohU$)Uf%>!Fe' );
define( 'NONCE_SALT',       'QXhw24@m$MEmd#ERE,Z*3v~0<~-nw!Z]i:PZ-$o<$U9Q5rMlX}B-og4_3v5/ErTt' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'al_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
