/**
 * Global theme functions
 */

/**
 * @description Exemplo ajax
 */
const exampleAjax = async () => {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { states, message } = await stateResponse.json()
	console.log(states, message)
}

window.onload = async () => {
	/**
	 * Contact form 7 alerts
	 */
	const form = document.querySelector('.wpcf7')
	if (form) {
		form.addEventListener('wpcf7mailsent', () => {
			Swal.fire({
				icon: 'success',
				title: 'Sucesso!',
				text: 'Mensagem enviada!',
			})
		})

		form.addEventListener('wpcf7mailfailed', () => {
			Swal.fire({
				icon: 'error',
				title: 'Ocorreu um erro!',
				text: 'Se o erro persistir, favor contatar o suporte.',
			})
		})
	}
}
$(document).ready(()=>{
	$(".hidden").hide();
	$(".up").hide();
	$('#faq-slider').lightSlider({
		adaptiveHeight:true,
        enableDrag: true,
		item:1,
		slideMargin:0,
		controls: false,
		loop:true
	});
	$('#depoimentos-slider').lightSlider({
		adaptiveHeight:true,
        enableDrag: true,
		item:3,
		slideMargin:0,
		controls: false,
		loop:true
	});
	$('#depoimentos-slider-mobile').lightSlider({
		adaptiveHeight:true,
        enableDrag: true,
		item:1,
		slideMargin:0,
		controls: false,
		loop:true
	});
	setTimeout(function(){
		$(".lSSlideOuter .lSPager").css({"padding-top":"30px"}); 
		$(".lSSlideOuter .lSPager").css({"height":"30px"}); 
		$(".lSSlideOuter .lSPager li a").css({"height": "14px", "border-radius": "8px", "width": "14px", "background": "#C6C6C6"});
		$(".lSSlideOuter .lSPager .active a").css({"height": "13px", "border-radius": "8px", "width": "32px", "background": "#00C3CD"});
		$(".lSSlideOuter .lSPager li").on("click", ()=>{
		});

	}, 1);
	if($('.all .top').attr('src')==''){
		$('.all .top').css("display", "none")
	}
	if($('.all .bottom').attr('src')==''){
		$('.all .bottom').css("display", "none")
	}
});