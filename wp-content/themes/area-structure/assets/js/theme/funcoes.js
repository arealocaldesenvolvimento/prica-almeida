function abrir(id){
	$("#faq-"+id).slideDown();
	$("#line-"+id).css("font-weight", "600");
	$("#up-"+id).show();
	$("#down-"+id).hide();
}
function fechar(id){
	$("#faq-"+id).slideUp();
	$("#line-"+id).css("font-weight", "400");
	$("#up-"+id).hide();
	$("#down-"+id).show();
}
function auto(id){
	if(!$("#faq-"+id).is(":visible")){
		abrir(id)
	}else{
		fechar(id)
	}
}