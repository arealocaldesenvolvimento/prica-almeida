<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#000000">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link
        rel="stylesheet"
        type="text/css"
        media="all"
        href="<?php echo get_template_directory_uri(); ?>/style.css"
    >
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightslider.css">
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightgallery.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-60374915-43"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-60374915-43');
    </script>
    <title>
        <?php echo is_front_page() || is_home() ? get_bloginfo('name') : wp_title(''); ?>
    </title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!-- Header -->
    <header role="heading" class="main-header">
        <div class="al-container">
            <div class="sub-menu">
                <div class="logo">
                    <a title="Prica Almeida" href="#wrapper">
                        <img src="<?= get_field('logomarca_menu', 'option')['url'] ?>" alt="Logo Prica Almeida">
                    </a>
                </div>
                <div class="main-menu">
                    <?php wp_nav_menu(array('menu'=> 'principal', 'theme_location' => 'principal', 'container' => false)); ?>
                </div>
            </div>
        </div>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
    <div class="whatsapp-link">
        <a href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")?>" target="_blank"><img src="<?= get_image_url("whatsapp-lateral.png") ?>"></a>
    </div>