<?php get_header() ?>

    <section class="banner" style="background-image: url('<?= get_image_url("banner-initial-2.png") ?>');" id="sobre">
        <div class="al-container">
            <div class="left">
				<h2>Yoga para crianças</h2><br>
                <p>As aulas de yoga visam compensar o mundo digital, trazendo consciência corporal, aprendizado psicomotor e principalmente mais concentração e harmonia nas mentes dos pequenos.</p>
                <div>
                    <a href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")?>" target="_blank" class="btn-white">SOLICITAR PROPOSTA</a>
                </div>
            </div>
        </div>
    </section>
    <section class="yoga-para-criancas" id="yoga-para-criancas">
        <div class="al-container">
            <h2 class="subtitle">Yoga para crianças</h2>
            <div class="text">
                <?= get_field("yoga-para-criancas")['texto_pre-beneficios']?>
            </div>
            <?php if(have_rows("yoga-para-criancas") && !empty(get_field("yoga-para-criancas")['beneficios'])){ ?>
            <div class="beneficios">
                <h2 class="subtitle">O QUE VOCÊ PODE ESPERAR:</h2>
                <div class="listas">
                    <ul>
                        <?php foreach(get_field("yoga-para-criancas")["beneficios"] as $chave => $valor): ?>
                            <?php if(round(count(get_field('yoga-para-criancas')["beneficios"])/2)-1>$chave){ ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            <?php }else if(round(count(get_field('yoga-para-criancas')["beneficios"])/2)-1==$chave) { ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            </ul>
                            <ul>
                            <?php }else{ ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            <?php } ?>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            <?php } ?>
            <div class="text">
                <?= get_field("yoga-para-criancas")['texto_pos-beneficios']?>
            </div>
            <div class="reiki-amor">
                <div class="btn-space">
                    <a class="btn-dark-green" target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")?>">
                        Peça agora uma proposta de parceria.
                    </a>
                </div>
            </div>
        </div>
        <div class="first-time">
            <div class="left">
                <h2 class="subtitle">Primeira vez no Yoga?</h2>
                <ul class="text">
                </ul>
                <a class="btn-red" target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")?>">
                    Quero meu orçamento
                </a>
            </div>
            <div class="right">
                <img src="<?= get_image_url("first-time.png")?>" alt="Imagem Decorativa Yoga">
            </div>
        </div>
    </section>
    <section class="curso-voce-e-os-mestres" id="curso-voce-e-os-mestres">
        <div class="al-container">
            <h2 class="subtitle">Curso Você e os Mestres</h2>
            <div class="text">
                <?= get_field("curso-voce-e-os-mestres")['texto_pre-beneficios']?>
            </div>
            <?php if(!empty(get_field("curso-voce-e-os-mestres")["beneficios"])){ ?>
            <div class="beneficios">
                <h2 class="subtitle">O QUE VOCÊ VAI APRENDER:</h2>
                <div class="listas">
                    <ul>
                        <?php foreach(get_field("curso-voce-e-os-mestres")["beneficios"] as $chave => $valor): ?>
                            <?php if(round(count(get_field('curso-voce-e-os-mestres')["beneficios"])/2)-1>$chave){ ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            <?php }else if(round(count(get_field('curso-voce-e-os-mestres')["beneficios"])/2)-1==$chave) { ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            </ul>
                            <ul>
                            <?php }else{ ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            <?php } ?>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            <?php } ?>
            <div class="text">
                <?= get_field("curso-voce-e-os-mestres")['texto_pos-beneficios']?>
            </div>
            <div class="btn-space">
                <a class="btn-soft-green" target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")?>">
                    Conecte-se com essa família celestial e se livre da solidão.
                </a>
            </div>
            <div class="protecao-garantia">
                <img src="<?= get_image_url('compra-segura.png') ?>" width="28px" height="28px" alt="Ícone Compra Segura">
                <span>Garantia: 7 dias e seu dinheiro de volta.</span>
            </div>
        </div>
    </section>
    <section class="quem-sou" id="conheca-nossa-professora">
        <div class="al-container">
            <h2 class="subtitle">Conheça nossa professora</h2>
            <div class="text-layout">
                <div class="left">
                    <?= get_field("quem_sou_eu") ?>
                </div>
                <div class="right">
                    <img src="<?= get_field("imagens_quem_sou_eu")['imagem_1']['url'] ?>" alt="<?= get_field("imagens_quem_sou_eu")['imagem_1']['title'] ?>">
                    <div class="all">
                        <img class="top" src="<?= get_field("imagens_quem_sou_eu")['imagem_2']['url'] ?>" alt="<?= get_field("imagens_quem_sou_eu")['imagem_2']['title'] ?>">
                        <img class="bottom" src="<?= get_field("imagens_quem_sou_eu")['imagem_3']['url'] ?>" alt="<?= get_field("imagens_quem_sou_eu")['imagem_3']['title'] ?>">
                    </div>
                </div>
            </div>
            <div class="under">
                <?= get_field("quem_sou_eu_parte_2") ?>
            </div>
        </div>
    </section>
    <section class="depoimentos" id="depoimentos">
        <div class="al-container">
            <h2 class="subtitle">Depoimentos</h2>
            <div class="depoimentos-lista">
                <ul id="depoimentos-slider">
                    <?php foreach(get_field("depoimentos") as $chave => $depoimento): ?>
                        <li>
                            <div class="depoimento">
                                <div class="content">
                                    <h3 class="nome"><?= $depoimento['pessoa'] ?></h3>
                                    <div class="avaliacao">
                                        <?php for($i=0; $i<$depoimento['avaliacao']; $i++): ?>
                                            <img src="<?= get_image_url("star-icon.png")?>" alt="Ícone estrela">
                                        <?php endfor; ?>
                                    </div>
                                    <div class="text">
                                        <?= $depoimento['comentario'] ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
                <ul id="depoimentos-slider-mobile">
                    <?php foreach(get_field("depoimentos") as $chave => $depoimento): ?>
                        <li>
                            <div class="depoimento">
                                <div class="content">
                                    <h3 class="nome"><?= $depoimento['pessoa'] ?></h3>
                                    <div class="avaliacao">
                                        <?php for($i=0; $i<$depoimento['avaliacao']; $i++): ?>
                                            <img src="<?= get_image_url("star-icon.png")?>" alt="Ícone estrela">
                                        <?php endfor; ?>
                                    </div>
                                    <div class="text">
                                        <?= $depoimento['comentario'] ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </section>
    <section class="yoga" id="yoga">
        <div class="al-container">
            <h2 class="subtitle">Yoga</h2>
            <div class="text">
                <?= get_field("yoga")['texto']?>
            </div>
            <div class="cards">
                <?php
                    foreach(get_field("yoga")["categorias"] as $chave => $valor):
                ?>
                    <div class="card">
                        <h3 class="subtitle"><?= $valor['titulo']?></h3>
                        <div class="atributos">
                            <img src="<?= get_image_url("agenda-icon.png") ?>" alt="Agenda">
                            <div><?= $valor['agenda']?></div>
                        </div>
                        <div class="atributos pratica">
                            <img src="<?= get_image_url("pratica-icon.png") ?>" alt="Ícone">
                            <div><?= $valor['pratica']?></div>
                        </div>
                        <a class="btn-green-a" target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")?>">
                            <div class="btn-green">
                                Agendar sessão
                            </div>
                        </a>
                    </div>
                <?php
                    endforeach;
                ?>
            </div>
        </div>
        <div class="first-time">
            <div class="left">
                <h2 class="subtitle">Primeira vez no Yoga?</h2>
                <p class="text">
                    Adquira um vale-presente para no valor de 150 reais e dê para quem você ama (presencial em Palhoça ou online):
                </p>
                <ul class="text">
                    <li>Uma mensalidade de yoga (4 aulas)</li>
                    <li>Uma sessão do curso você e os mestres </li>
                    <li>Uma sessão de yoga para crianças </li>
                </ul>
                <a class="btn-red" target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")?>">
                    Quero minha aula gratuita!
                </a>
            </div>
            <div class="right">
                <img src="<?= get_image_url("first-time.png")?>" alt="Imagem Decorativa Yoga">
            </div>
        </div>
        <?php
            if(have_rows("aula")) {
        ?>
        <div class="aula">
            <div class="al-container">
                <h2 class="subtitle">Veja se essa aula é pra você:</h2>
                <div class="listas">
                    <div class="left">
                        <ul>
                            <?php
                                if(have_rows("aula")) {
                                    foreach(get_field("aula") as $chave => $valor):
                                            if($valor["topico"]['para_voce']=="Aula é para você"){?>
                                                <li>
                                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                                    <?= $valor["topico"]["texto"]?>
                                                </li>
                                    <?php   }
                                    endforeach; }
                                ?>
                        </ul>
                    </div>
                    <div class="right">
                        <ul>
                            <?php
                                if(have_rows("aula")) {
                                    foreach(get_field("aula") as $chave => $valor):
                                        if($valor["topico"]['para_voce']=="Aula não é para você"){?>
                                            <li>
                                                <img src="<?= get_image_url("uncorrect-li.png") ?>" alt="Ícone Errado">
                                                <?= $valor["topico"]["texto"]?>
                                            </li>
                                <?php   }
                                    endforeach; }
                                ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </section>
    <?php
        if(have_rows("perguntas_frequentes")){
    ?>
        <section class="faq">
            <div class="al-container">
                <h2 class="subtitle">Perguntas Frequentes</h2>
                <div id="faq-slider">
                <ul>
                    <?php
                        if(have_rows("perguntas_frequentes")){
                            foreach(get_field('perguntas_frequentes') as $chave => $diferencial){
                        ?>
                            <li class="faqli" onclick="auto(<?= $chave ?>)">
                                <div class="pergunta">
                                    <div class="line" id="line-<?= $chave ?>">
                                        <?= $diferencial['pergunta'] ?>
                                        <div class="img">
                                            <div class="down" id="down-<?= $chave ?>">
                                                <img src="<?= get_image_url('down.png') ?>" alt="Ícone Down">
                                            </div>
                                            <div class="up" id="up-<?= $chave ?>">
                                                <img src="<?= get_image_url('up.png') ?>" alt="Ícone Up">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hidden" id="faq-<?= $chave ?>">
                                        <?= $diferencial['resposta'] ?>
                                    </div>
                                </div>
                            </li>
                            <?php
                                if(($chave+1)%9==0){
                                    echo "</ul> <ul>";
                                }
                            ?>
                    <?php } }?>
                </ul>
            </div>
            </div>
        </section>
    <?php }?>
    <!-- <section class="reiki" id="reiki">
        <div class="al-container">
            <h2 class="subtitle">Reiki</h2>
            <div class="text">
                <?= get_field("reiki")['texto_pre-beneficios']?>
            </div>
            <?php if(have_rows("reiki") && !empty(get_field("reiki")['beneficios'])){ ?>
            <div class="beneficios">
                <h2 class="subtitle">Benefícios do Reiki:</h2>
                <div class="listas">
                    <ul>
                        <?php foreach(get_field("reiki")["beneficios"] as $chave => $valor): ?>
                            <?php if(round(count(get_field('reiki')["beneficios"])/2)-1>$chave){ ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            <?php }else if(round(count(get_field('reiki')["beneficios"])/2)-1==$chave) { ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            </ul>
                            <ul>
                            <?php }else{ ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            <?php } ?>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            <?php } ?>
            <div class="text">
                <?= get_field("reiki")['texto_pos-beneficios']?>
            </div>
            <div class="reiki-amor">
                <h2 class="subtitle">Reiki do Amor Próprio</h2>
                <div class="text"><?= get_field("reiki")['reiki_amor_proprio']?></div>

                <div class="btn-space">
                    <a class="btn-dark-green" target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")?>">
                        Agendar sessão
                    </a>
                </div>
            </div>
        </div>
    </section> -->
    <!-- <section class="cura-arcturiana" id="cura">
        <div class="al-container">
            <h2 class="subtitle">Cura Arcturiana</h2>
            <div class="text">
                <?= get_field("cura_arcturiana")['texto_pre-beneficios']?>
            </div>
            <?php if(!empty(get_field("cura_arcturiana")["beneficios"])){ ?>
            <div class="beneficios">
                <h2 class="subtitle">Benefícios do Sistema Arcturiano de Cura Multidimensional:</h2>
                <div class="listas">
                    <ul>
                        <?php foreach(get_field("cura_arcturiana")["beneficios"] as $chave => $valor): ?>
                            <?php if(round(count(get_field('cura_arcturiana')["beneficios"])/2)-1>$chave){ ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            <?php }else if(round(count(get_field('cura_arcturiana')["beneficios"])/2)-1==$chave) { ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            </ul>
                            <ul>
                            <?php }else{ ?>
                                <li>
                                    <img src="<?= get_image_url("correct-li.png") ?>" alt="Ícone Certo">
                                    <?= $valor['beneficio'] ?>
                                </li>
                            <?php } ?>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            <?php } ?>
            <div class="text">
                <?= get_field("cura_arcturiana")['texto_pos-beneficios']?>
            </div>
            <div class="btn-space">
                <a class="btn-soft-green" target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")?>">
                    Agendar sessão
                </a>
            </div>
        </div>
    </section> -->
<?php get_footer() ?>
