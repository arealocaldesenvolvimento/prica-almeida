<?php

/**
 * Imports.
 */
include_once ABSPATH.'/wp-load.php';

/**
 * Fetch all states.
 */
function getStates(): WP_REST_Response
{
    try {
        global $wpdb;
        $states = $wpdb->get_results('SELECT id, uf FROM al_estados');

        if (!empty($states)) {
            return new WP_REST_Response([
                'status' => 200,
                'states' => $states,
                'message' => 'Estados obtidos com sucesso',
            ]);
        }

        return new WP_REST_Response([
            'status' => 500,
            'message' => 'Erro ao obter os estados',
        ]);
    } catch (\Throwable $th) {
        return new WP_REST_Response([
            'status' => 500,
            'message' => 'Um erro inesperado ocorreu: '.$th->getMessage(),
        ]);
    }
}
